#!/usr/bin/env python
from distutils.core import setup

setup(name='sdf',
      version='1.04',
      description='Light SDF reader object (used by InChI generation example)',
      long_description="""\
Parsing MDL SD file records and collecting information
(atomic, bonding, etc.) for further use
The implementation is very 'light' and is provided for
illustrative purposes only.
""",
      author='The InChI Trust',
      url='https://searchcode.com/codesearch/raw/19269206/',
      scripts = [],
      packages = ['sdf'],
      classifiers=['License :: OSI Approved :: GNU General Public License (GPL)',
                   'Operating System :: Unix',
                   'Programming Language :: Python',
                   'Topic :: Scientific/Engineering :: Bio-Informatics',
                   'Topic :: Software Development :: Libraries :: Python Modules'],
      license='GNU General Public License (GPL)'
     )

